package com.tt.plat.core.impl;



import com.tt.plat.core.api.persistence.IGenericDao;
import com.tt.plat.core.api.persistence.IPersistentEntity;
import com.tt.plat.core.impl.exception.SearchException;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by dell on 14-1-16.
 */
public class GenericOpenJpaDao<T extends IPersistentEntity, PK extends Serializable> implements IGenericDao<T, PK> {
    protected final Logger log = Logger.getLogger(getClass().getName());
    private Class<T> persistentClass;
    protected EntityManager entityManager;

    public void setEntityManager(EntityManager em) {
        entityManager = em;

    }

    public GenericOpenJpaDao() {

        //Get "T" and assign it to this.entityClass
        Object obj= this.getClass().getGenericSuperclass();
        ParameterizedType genericSuperclass = (ParameterizedType) this.getClass().getGenericSuperclass();
        java.lang.reflect.Type type = genericSuperclass.getActualTypeArguments()[0];
        if (type instanceof Class) {
            this.persistentClass = (Class<T>) type;
        } else if (type instanceof ParameterizedType) {
            this.persistentClass = (Class<T>) ((ParameterizedType)type).getRawType();
        }
    }

    public void setPersistentClass(Class cls){
        this.persistentClass=cls;
    }

    /**
     * Constructor that takes in a class to see which type of entity to persist.
     * Use this constructor when subclassing.
     *
     * @param persistentClass the class type you'd like to persist
     */
    public GenericOpenJpaDao(final Class<T> persistentClass) {
        this.persistentClass = persistentClass;
    }

    @Override
    public List<T> getAll(String className) {
        final Query query = entityManager.createQuery("select c from " + className + " c ");
        final List<T> resultList = query.getResultList();
        return resultList;
    }

    public List<T> getAll(){

        return getAll(persistentClass.getName());
    }

    @Override
    public List<T> getAllDistinct() {
        return null;
    }

    @Override
    public List<T> search(String searchTerm) throws SearchException {
        return null;
    }

    @Override
    public T get(PK id) {
        return entityManager.find(persistentClass, id);
    }

    @Override
    public boolean exists(PK id) {
        return false;
    }

    @Override
    public T save(T object) {
        entityManager.persist(object);
        entityManager.flush();
        return object;
    }

    @Override
    public void remove(T object) {
        entityManager.remove(object);
        entityManager.flush();
    }

    @Override
    public void remove(PK id) {
        Object object = entityManager.find(persistentClass, id);
        entityManager.remove(object);
        entityManager.flush();



    }


    /**
     * 按HQL查询唯一对象.
     */
    public <T> T findUnique(String hql, Object... values) {
        List list = this.find(hql,values);
        if(list!=null&&list.size()>0){

            return (T)list.get(0);
        }
        return null;
    }

    @Override
    public List<T> findByNamedQuery(String queryName, Map<String, Object> queryParams) {

        return null;
    }

    @Override
    public void reindex() {

    }

    @Override
    public void reindexAll(boolean async) {

    }

    public List find(String hql, Object... values) {
        return createQuery(hql, values).getResultList();
    }

    /**
     * 根据查询函数与参数列表创建Query对象,后续可进行更多处理,辅助函数.
     */
    protected Query createQuery(String queryString, Object... values) {
        Query queryObject = entityManager.createQuery(queryString);
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                queryObject.setParameter(i+1, values[i]);
            }
        }
        return queryObject;
    }


}
