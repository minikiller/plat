package com.tt.plat.core.web.api.modules;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-3-7
 * Time: 上午11:45
 *8Plat系统模板
 */
public interface IPlatSystemModule extends ISystemModule{


    /**
     * 获取目标TAG（能够安装到此系统上的module根据tag来判断）
     * @return
     */
    public String[] getTargetTag();
}
