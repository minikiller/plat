package com.tt.plat.core.web.api.modules;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-17
 * Time: 下午2:23
 * 模块接口
 */
public interface IModule {
    /**
     * 模块名称
     * @return
     */
    String getName();

    /**
     * 模块图标
     * @return
     */
    String getIcon();

    /**
     * 模块tag
     * @return
     */
    String getTag();

//    /**
//     * 模块组件
//     * @return
//     */
//    Component getComponent();
}
