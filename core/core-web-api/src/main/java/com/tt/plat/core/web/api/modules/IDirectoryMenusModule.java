package com.tt.plat.core.web.api.modules;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-3-10
 * Time: 下午12:40
 * 目录菜单模块（此菜单下面包含多个菜单模块）
 */
public interface IDirectoryMenusModule extends IModule{


    /**
     * 获取可以附加到这个菜单目录下的菜单的tag
     * @return
     */
    public String[] getTargetTag();
}
