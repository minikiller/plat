package com.tt.plat.core.web.api.modules;

import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-19
 * Time: 上午11:24
 * To change this template use File | Settings | File Templates.
 */
public interface IPlatSession {


    /**
     * 获取平台session中的内容
     * @return
     */
    public Object getObj();


    /**
     * 设置平台session中的内容
     * @param user
     */
    public void setObj(Object user);


    /**
     * 是否有给定的角色
     * @param role
     * @return
     */
    public boolean hasRole(String role);

}
