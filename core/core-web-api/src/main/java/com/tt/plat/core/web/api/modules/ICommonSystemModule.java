package com.tt.plat.core.web.api.modules;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-3-7
 * Time: 上午11:38
 * 普通的系统模块
 */
public interface ICommonSystemModule extends ISystemModule {


    /**
     * 获取系统的链接
     * @return
     */
    public String getUrl();
}
