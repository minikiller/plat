package com.tt.plat.core.web.imp;



import com.tt.plat.core.web.api.modules.IModule;
import com.tt.plat.core.web.api.modules.ISystemModule;
import com.tt.plat.core.web.imp.manager.SystemModuleManager;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-3-7
 * Time: 下午3:39
 * To change this template use File | Settings | File Templates.
 */
public class Plat {

    private Plat() {

    }

    private static Plat install;

    private ISystemModule systemModule;


    public static Plat getInstall() {
        if (install == null) {
            install = new Plat();

        }


        return install;
    }



    public void setCurrSystem(ISystemModule systemModule){
        this.systemModule=systemModule;
    }

    public ISystemModule getCurrSystem(){

        return  systemModule;
    }


    /**
     * 获取当前菜单
     * @return
     */
    public List<IModule>  getCurrMenus(){

        ISystemModule systemM =getCurrSystem();
        if(systemM!=null){
            return SystemModuleManager.getInstall().getSystemMenus(systemM);
        }

        return null;
    }


}

