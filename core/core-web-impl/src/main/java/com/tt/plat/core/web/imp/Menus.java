package com.tt.plat.core.web.imp;

import org.apache.wicket.Page;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-1-24
 * Time: 下午6:18
 * To change this template use File | Settings | File Templates.
 */
public class Menus {

    private String no;

    private String name;

    private String icon;

    private Class<? extends Page> page;

    private List<Menus> subMenus = new ArrayList<Menus>();

    private Menus parent;

    public Class<? extends Page> getPage() {
        return page;
    }

    public void setPage(Class<? extends Page> page) {
        this.page = page;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<Menus> getSubMenus() {
        return subMenus;
    }

    public void setSubMenus(List<Menus> subMenus) {
        this.subMenus = subMenus;
    }

    public Menus getParent() {
        return parent;
    }

    public void setParent(Menus parent) {
        this.parent = parent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Menus menus = (Menus) o;

        if (no != null ? !no.equals(menus.no) : menus.no != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return no != null ? no.hashCode() : 0;
    }
}
