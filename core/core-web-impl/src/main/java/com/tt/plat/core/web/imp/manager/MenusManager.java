package com.tt.plat.core.web.imp.manager;



import com.tt.plat.core.web.api.modules.IDirectoryMenusModule;
import com.tt.plat.core.web.api.modules.IMenusModule;
import com.tt.plat.core.web.api.modules.IModule;
import com.tt.plat.core.web.imp.Menus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-1-24
 * Time: 下午6:47
 * To change this template use File | Settings | File Templates.
 */
public class MenusManager {

    //菜单集合
    private List<IMenusModule> menusModuleList = new ArrayList<IMenusModule>();

    private List<IDirectoryMenusModule> directoryMenusModuleList = new ArrayList<IDirectoryMenusModule>();

    private Menus root = new Menus();

    private static MenusManager install;

    private MenusManager() {
        root.setName("8plat");
    }

    public synchronized static MenusManager getInstall() {
        if (install == null) {
            install = new MenusManager();
        }
        return install;
    }

    public Menus getRootMenus() {

        return root;
    }

    public void add(IMenusModule menusModule) {
        menusModuleList.add(menusModule);
    }

    public void remove(IMenusModule menusModule) {
        menusModuleList.remove(menusModule);
    }

    public void add(IDirectoryMenusModule directoryMenusModule) {
        directoryMenusModuleList.add(directoryMenusModule);
    }

    public void remove(IDirectoryMenusModule directoryMenusModule) {
        directoryMenusModuleList.remove(directoryMenusModule);
    }


    /**
     * 通过tag获取对应的菜单模块
     *
     * @param tags
     * @return
     */
    public List<IModule> findMenusByTag(String... tags) {
        List<IModule> modules = new ArrayList<IModule>();

        //获取对应的tag的菜单
        if (menusModuleList != null) {
            for (IMenusModule menusModule : menusModuleList) {

                for(String tag : tags){
                    if(tag.equals( menusModule.getTag())){
                        modules.add(menusModule);
                    }
                }

            }
        }

        //获取对应的tag的菜单目录
        if(directoryMenusModuleList!=null){
            for (IDirectoryMenusModule directoryMenusModule : directoryMenusModuleList) {

                for(String tag : tags){
                    if(tag.equals( directoryMenusModule.getTag())){
                        modules.add(directoryMenusModule);
                    }
                }

            }
        }
        return modules;
    }


}
