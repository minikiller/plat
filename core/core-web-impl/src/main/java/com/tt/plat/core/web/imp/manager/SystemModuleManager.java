package com.tt.plat.core.web.imp.manager;




import com.tt.plat.core.web.api.modules.ICommonSystemModule;
import com.tt.plat.core.web.api.modules.IModule;
import com.tt.plat.core.web.api.modules.IPlatSystemModule;
import com.tt.plat.core.web.api.modules.ISystemModule;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-3-7
 * Time: 上午11:58
 * 系统模块管理者
 */
public class SystemModuleManager {

    //普通系统模块集合
    private static List<ICommonSystemModule> commonSystemModuleList = new ArrayList<ICommonSystemModule>();

    //8plat系统模块集合
    private static List<IPlatSystemModule> plat8SystemModuleList = new ArrayList<IPlatSystemModule>();


    private SystemModuleManager(){

    }

    private static SystemModuleManager install;

    public static SystemModuleManager getInstall(){
             if(install==null){
                 install = new SystemModuleManager();
             }
        return install;
    }

    /**
     * 普通系统模块
     */
    public final static int COMMON_SYSTEM_MODULE = 1;

    /**
     * 8plat系统模块
     */
    public final static int PLAT8_SYSTEM_MODULE = 2;


    public  <T extends ISystemModule> List<T> getSystemModuleList(int systemFlag) {
        if (systemFlag == COMMON_SYSTEM_MODULE) {

            return (List<T>) commonSystemModuleList;
        } else if (systemFlag == PLAT8_SYSTEM_MODULE) {
            return (List<T>) plat8SystemModuleList;
        } else {
                return null;
        }
    }

    public void add(IPlatSystemModule systemModule){
        plat8SystemModuleList.add(systemModule);
    }

    public void add(ICommonSystemModule systemModule){
        commonSystemModuleList.add(systemModule);
    }

    public void remove(IPlatSystemModule systemModule){
        plat8SystemModuleList.remove(systemModule);
    }

    public void remove(ICommonSystemModule systemModule){
        commonSystemModuleList.remove(systemModule);
    }


    /**
     * 获取到系统的菜单结构
     * @param module
     */
    public List<IModule> getSystemMenus(ISystemModule module){

          if(module instanceof IPlatSystemModule){
              return getPlat8SystemMenus((IPlatSystemModule)module);
          }

        return  null;

    }


    /**
     * 获取到类型为plat8系统模块的菜单
     * @param systemModule
     * @return
     */
    private List<IModule> getPlat8SystemMenus(IPlatSystemModule systemModule){

        String[] targetTags  =systemModule.getTargetTag();
        if(targetTags!=null){
            return MenusManager.getInstall().findMenusByTag(targetTags);
        }
        return null;
    }





}
