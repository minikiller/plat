package com.tt.plat.core.web.imp;

import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.feedback.IFeedbackMessageFilter;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-18
 * Time: 上午11:27
 * To change this template use File | Settings | File Templates.
 */
public class MessageFeebackPanel extends FeedbackPanel {

    private static final long serialVersionUID = 1L;

    Boolean hasError = false;

    public MessageFeebackPanel(String id)
    {
        this(id,null);
    }

    public MessageFeebackPanel(String id, IFeedbackMessageFilter filter)
    {
        super(id,filter);

        WebMarkupContainer messagesContainer = (WebMarkupContainer)get("feedbackul");
        messagesContainer.add(new AttributeAppender("class",new Model<String>("alert fade in alert-danger")," "));
    }

    @Override
    protected String getCSSClass(FeedbackMessage message)
    {
        String cssClass="muted";
        switch(message.getLevel())
        {
            case FeedbackMessage.ERROR:
                cssClass="text-error";
                break;
            case FeedbackMessage.FATAL:
                cssClass="text-error";
                break;
            case FeedbackMessage.INFO:
                cssClass="text-info";
                break;
            case FeedbackMessage.SUCCESS:
                cssClass="text-success";
                break;
            case FeedbackMessage.WARNING:
                cssClass="text-warning";
                break;
        }
        return cssClass;
    }

    public void addSuccess(Serializable message){
        super.success(message);
    }

    public void addError(Serializable message)
    {
        super.error(message);
        hasError = true;
    }
    public Boolean hasErrors()
    {
        return hasError;
    }
    public void clearErrors()
    {
        hasError=false;
    }
}
