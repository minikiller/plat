package com.tt.plat.bootstrapwicket;


import com.tt.plat.core.web.api.modules.IPageModule;
import org.apache.wicket.Page;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-18
 * Time: 上午10:54
 * To change this template use File | Settings | File Templates.
 */
public class HomePageModule implements IPageModule {

    private Class<Page> page;
    @Override
    public Class<? extends Page> getPageClass() {
        return page;  //To change body of implemented methods use File | Settings | File Templates.
    }



    public void setPage(Class<Page> page) {
        this.page = page;
    }

    @Override
    public String getName() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getIcon() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getTag() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }


}
