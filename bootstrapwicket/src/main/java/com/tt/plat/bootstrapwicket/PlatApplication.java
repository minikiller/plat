package com.tt.plat.bootstrapwicket;




import com.tt.plat.bootstrapwicket.utils.JNDIHelper;
import com.tt.plat.core.web.api.modules.IndexPageModule;
import com.tt.plat8.admin.security.UserRolesAuthorizer;
import com.tt.plat8.admin.security.UserSession;
import org.apache.wicket.Page;
import org.apache.wicket.Session;
import org.apache.wicket.authroles.authorization.strategies.role.RoleAuthorizationStrategy;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;

import java.io.IOException;


/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-1-22
 * Time: 下午3:42
 * To change this template use File | Settings | File Templates.
 */
public class PlatApplication extends WebApplication{



    @Override
    public Class<? extends Page> getHomePage() {
        IndexPageModule mainPageProvider1=null;
        try {
           mainPageProvider1 =  JNDIHelper.getJNDIServiceForName(IndexPageModule.class.getName());
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return mainPageProvider1.getPageClass();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected void init() {
        super.init();    //To change body of overridden methods use File | Settings | File Templates.
        // Sets Wicket Markup settings for this application
        getMarkupSettings().setStripWicketTags(true);
        getMarkupSettings().setDefaultMarkupEncoding("UTF-8");

        // Sets Wicket Request Cycle settings for this application
        getRequestCycleSettings().setBufferResponse(true);
        getRequestCycleSettings().setGatherExtendedBrowserInfo(true);
        getRequestCycleSettings().setResponseRequestEncoding("UTF-8");

         // 角色认证策略
        getSecuritySettings().setAuthorizationStrategy(
                new RoleAuthorizationStrategy(new UserRolesAuthorizer()));






        // getComponentInstantiationListeners().add(new SpringComponentInjector(this));
    }

    @Override
    public Session newSession(Request request, Response response) {
        return new UserSession(request);
    }
}
