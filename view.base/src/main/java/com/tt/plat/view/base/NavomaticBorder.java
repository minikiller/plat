package com.tt.plat.view.base;


import com.tt.plat.view.base.panel.HeaderPanel;
import com.tt.plat.view.base.panel.MenusPanel;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.border.Border;
import org.apache.wicket.settings.IJavaScriptLibrarySettings;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-1-24
 * Time: 上午10:54
 * To change this template use File | Settings | File Templates.
 */
public class NavomaticBorder extends Border {

//    @Named("pageProviders")
//    @Inject
//    private java.util.List<PageModule> pageProvider;
    public NavomaticBorder(String id) {
        super(id);


        add(new MenusPanel("menus").setRenderBodyOnly(true));
        add(new HeaderPanel("header")).setRenderBodyOnly(true);


    }

    @Override
    public void renderHead(IHeaderResponse response) {
        IJavaScriptLibrarySettings javaScriptSettings =
                getApplication().getJavaScriptLibrarySettings();
//        response.render(JavaScriptHeaderItem.
//                forReference(javaScriptSettings.getJQueryReference()));

//        response.render(CssHeaderItem.forUrl("res/metro/metro-bootstrap.min.css"));
//        response.render(CssHeaderItem.forUrl("res/metro/metro-responsive.min.css"));
//        response.render(CssHeaderItem.forUrl("res/css/plat8.css"));
////
//        response.render(JavaScriptHeaderItem.forUrl("res/jquery-1.10.2.min.js"));
//        response.render(JavaScriptHeaderItem.forUrl("res/jquery.widget.min.js"));


//        response.render(JavaScriptHeaderItem.forUrl("res/metro/metro.min.js"));
    }
}
