package com.tt.plat.view.base.panel;


import com.tt.plat.core.web.api.modules.IModule;
import com.tt.plat.core.web.imp.Plat;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-1-23
 * Time: 下午6:33
 * To change this template use File | Settings | File Templates.
 */
public class MenusPanel extends Panel {





    public MenusPanel(String id) {
        super(id);

      List<IModule> menusList = Plat.getInstall().getCurrMenus();

//      final   ListView<Menus> links = new ListView<Menus>("testmenusItems",menusList ) {
//            @Override
//            protected void populateItem(ListItem<Menus> item) {
//
//                item.add( new ItemPanel("submenusItem",item.getModelObject()));
//               // new ItemPanel("menusItem",item.getModelObject())
//
//            }
//        };
//        //add(new ItemPanel("subtest",menusList.get(0)));
//        add(links);

        RepeatingView listItems = new RepeatingView("menusItems");
        if(menusList!=null){

            int i=0;
               for(IModule menus:menusList){

                   listItems.add( new ItemPanel(listItems.newChildId(),menus,i));
                   i++;
               }
        }



        add(listItems);
    }




}
