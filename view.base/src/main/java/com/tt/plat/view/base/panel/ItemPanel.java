package com.tt.plat.view.base.panel;


import com.tt.plat.core.web.api.modules.IDirectoryMenusModule;
import com.tt.plat.core.web.api.modules.IMenusModule;
import com.tt.plat.core.web.api.modules.IModule;
import com.tt.plat.core.web.imp.manager.MenusManager;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.authroles.authorization.strategies.role.metadata.MetaDataRoleAuthorizationStrategy;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.link.StatelessLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-1-24
 * Time: 下午6:54
 * To change this template use File | Settings | File Templates.
 */
public class ItemPanel extends Panel {


    private WebMarkupContainer subMenusContainer = new WebMarkupContainer("submenusContainer");

    private WebMarkupContainer menusContainer = new WebMarkupContainer("menusContainer");

    public ItemPanel(String id,final IModule menus,final int index) {
        super(id);


        String selectedMenus = (String) getSession().getAttribute("menus");


        int mi = 0; //当前被选中的菜单下标
        int si = 0; //当前被选中的子菜单下标
        if(selectedMenus!=null){
               String[] indexStrs = selectedMenus.split(",");
            mi = Integer.parseInt(indexStrs[0]);

            si = Integer.parseInt(indexStrs[1]);


            if(index==mi){ //将当前下标设置为打开状态
                menusContainer.add(new AttributeModifier("class","open-default"));
            }
        }



        //子菜单模块
        List<IModule> submenus =null;

         //菜单链接
        Link menusLink = null;
        if(menus instanceof IDirectoryMenusModule){  // 如果菜单是目录
            menusLink = new StatelessLink("menus") {
                @Override
                public void onClick() {
                }

                protected CharSequence getURL() {

                    return "javascript:void(0);";
                }
            };

            //获取目录下面的子菜单
            submenus = MenusManager.getInstall().findMenusByTag(((IDirectoryMenusModule)menus).getTargetTag());
        }else{
            menusLink = new BookmarkablePageLink("menus", ((IMenusModule)menus).getPageClass());
        }


        Label iconLabel =new Label("name", " <i class=\"" + menus.getIcon() + "\"></i>" + menus.getName());
        iconLabel.setEscapeModelStrings(false);
        iconLabel.setRenderBodyOnly(true);
        menusLink.add(iconLabel);
        menusContainer.add(menusLink);





        //subMenusContainer.setOutputMarkupId(true);

        //菜单消息数量
        Label msgNumLabel = new Label("msgNum","");
        msgNumLabel.setOutputMarkupId(true);
        msgNumLabel.setVisible(false);//暂时隐藏
        menusLink.add(msgNumLabel);




        //设置子菜单

        ListView<IModule> links = new ListView<IModule>("menusItems", submenus) {

            int i=0;
            @Override
            protected void populateItem(final ListItem<IModule> item) {
              //  BookmarkablePageLink submenusLink = new BookmarkablePageLink("menus", ((IMenusModule)item.getModelObject()).getPageClass());
               StatelessLink  submenusLink = new StatelessLink("menus") {
                   @Override
                   public void onClick() {
                           getSession().setAttribute("menus",index+","+i);
                       setResponsePage(((IMenusModule) item.getModelObject()).getPageClass());
                   }


               };
                i++;
                submenusLink.setOutputMarkupId(true);
                submenusLink.add(new Label("name", " <i class=\"" + item.getModelObject().getIcon() + "\"></i>" + item.getModelObject().getName()).setRenderBodyOnly(true).setEscapeModelStrings(false));
                item.add(submenusLink);


//                    String role = RolesManager.getInstall().getRoleByClassName( ((IMenusModule)item.getModelObject()).getPageClass().getName());
//                if(role!=null){
//                    MetaDataRoleAuthorizationStrategy.authorize(((IMenusModule)item.getModelObject()).getPageClass(), role);
//                }



            }
        };
      //  links.setOutputMarkupId(true);

        subMenusContainer.setVisible(submenus.size()>0?true:false);
        subMenusContainer.add(links);

        menusContainer.add(subMenusContainer);

        add(menusContainer);

    }
}
