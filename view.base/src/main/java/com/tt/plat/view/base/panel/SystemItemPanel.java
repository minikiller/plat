package com.tt.plat.view.base.panel;


import com.tt.plat.core.web.api.modules.ISystemModule;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.StatelessLink;
import org.apache.wicket.markup.html.panel.Panel;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-3-7
 * Time: 下午4:07
 * To change this template use File | Settings | File Templates.
 */
public class SystemItemPanel extends Panel {


    public SystemItemPanel(String id,final ISystemModule systemModule) {
        super(id);


        //系统链接
        StatelessLink systemLink = new StatelessLink("systemLink") {
            @Override
            public void onClick() {
                     System.out.println("zzzz---"+systemModule.getName());
            }
        };
        this.add(systemLink);
        //系统名称
        Label systemNameLabel = new Label("systemName",systemModule.getName());

        systemLink.add(systemNameLabel);
             String icon = systemModule.getIcon();

        if(icon==null||icon.trim().equals("")){
            icon="icon-desktop";
        }  else{
            icon = systemModule.getIcon();
        }
        //系统图标
        Label systemIcon = new Label("systemIcon","<i class='"+icon+"'></i>");
        systemIcon.setEscapeModelStrings(false);
        systemIcon.setRenderBodyOnly(true);
        systemLink.add(systemIcon);
        //class="icon-desktop"
    }
}
