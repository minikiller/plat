package com.tt.plat.view.base;


import com.tt.plat.core.web.api.modules.ICommonSystemModule;
import com.tt.plat.core.web.api.modules.IPlatSystemModule;
import com.tt.plat.core.web.imp.manager.SystemModuleManager;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-3-7
 * Time: 上午11:58
 * 系统模块管理者
 */
public class SystemModuleListener {



    /**
     * 监听到注入普通系统模块
     *
     * @param systemModule
     */
    public void bind(ICommonSystemModule systemModule) {

        SystemModuleManager.getInstall().add(systemModule);


    }

    /**
     * 监听到注入8plat系统模块
     *
     * @param systemModule
     */
    public void bind(IPlatSystemModule systemModule) {
        SystemModuleManager.getInstall().add(systemModule);

    }

    /**
     * 普通系统模块被移除
     *
     * @param systemModule
     */
    public void unbind(ICommonSystemModule systemModule) {
        SystemModuleManager.getInstall().remove(systemModule);
    }

    /**
     * 8plat系统模块被移除
     *
     * @param systemModule
     */
    public void unbind(IPlatSystemModule systemModule) {
        SystemModuleManager.getInstall().remove(systemModule);
    }
}
