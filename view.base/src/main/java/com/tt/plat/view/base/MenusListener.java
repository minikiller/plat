package com.tt.plat.view.base;


import com.tt.plat.core.web.api.modules.IDirectoryMenusModule;
import com.tt.plat.core.web.api.modules.IMenusModule;
import com.tt.plat.core.web.imp.manager.MenusManager;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-1-24
 * Time: 下午6:17
 * 菜单监听者
 */
public class MenusListener {



    /**
     * 监听到菜单
     * @param IMenusModule
     */
    public void bind(IMenusModule IMenusModule){

        MenusManager.getInstall().add(IMenusModule);

    }



    /**
     * 菜单被移除
     * @param IMenusModule
     */
    public void unbind(IMenusModule IMenusModule){
        MenusManager.getInstall().remove(IMenusModule);

    }


    /**
     * 监听到菜单目录
     * @param menusModule
     */
    public void bind(IDirectoryMenusModule menusModule){

        MenusManager.getInstall().add(menusModule);

    }

    /**
     * 菜单目录被移除
     * @param menusModule
     */
    public void unbind(IDirectoryMenusModule menusModule){
        MenusManager.getInstall().remove(menusModule);

    }





}
