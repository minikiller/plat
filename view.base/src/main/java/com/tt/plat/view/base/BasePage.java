package com.tt.plat.view.base;

import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-1-24
 * Time: 上午11:06
 * To change this template use File | Settings | File Templates.
 */
public class BasePage extends WebPage {


   private  NavomaticBorder navomaticBorder;
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor
     */
    public BasePage()
    {
        this(new PageParameters());
    }

    /**
     * Constructor
     *
     * @param pageParameters
     */
    public BasePage(final PageParameters pageParameters)
    {
        super(pageParameters);

        navomaticBorder =new NavomaticBorder("plat");
        super.add(navomaticBorder);





        // add(new MenusPanel("menus").setRenderBodyOnly(true));
        explain();
    }



    public MarkupContainer add(org.apache.wicket.Component component){
        navomaticBorder.add(component);
        return navomaticBorder;
    }

    /**
     * Construct.
     *
     * @param model
     */
    public BasePage(IModel<?> model)
    {
        super(model);
    }

    /**
     * Override base method to provide an explanation
     */
    protected void explain()
    {
    }
}
