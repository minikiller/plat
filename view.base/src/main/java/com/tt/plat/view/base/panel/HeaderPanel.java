package com.tt.plat.view.base.panel;


import com.tt.plat.core.web.api.modules.IPlatSystemModule;
import com.tt.plat.core.web.imp.manager.SystemModuleManager;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-1-23
 * Time: 下午6:26
 * To change this template use File | Settings | File Templates.
 */
public class HeaderPanel extends Panel {

    public HeaderPanel(String id) {
        super(id);

      List<IPlatSystemModule> systemModuleList = SystemModuleManager.getInstall().getSystemModuleList(SystemModuleManager.PLAT8_SYSTEM_MODULE);


        RepeatingView listItems = new RepeatingView("systemListView");

        if(systemModuleList!=null){
                 for(IPlatSystemModule systemModule : systemModuleList){
                       SystemItemPanel systemItemPanel = new SystemItemPanel(listItems.newChildId(),systemModule);
                     systemItemPanel.setRenderBodyOnly(true);

                     listItems.add(systemItemPanel);

                 }
        }


        this.add(listItems);
    }
}
