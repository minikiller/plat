package com.tt.plat8.admin.entities;

import com.tt.plat.core.impl.PersistentEntity;
import com.tt.plat8.admin.api.IFunction;


import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-20
 * Time: 下午2:33
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "P_FUNCTION")
public class Function extends PersistentEntity implements IFunction {

    private String name;

    private String icon;

    private String className;

    private String descript;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }
}
