package com.tt.plat8.admin.entities;



import com.tt.plat.core.impl.PersistentEntity;
import com.tt.plat8.admin.api.IRoleCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-19
 * Time: 下午12:00
 * 角色代码实体
 */

@Entity
@Table(name = "P_ROLE_CODE")
public class RoleCode extends PersistentEntity implements IRoleCode {

    private String roleCode;

    private String description;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="roleCode")
    private List<UserRole> userRoles;

    @OneToMany(cascade = CascadeType.ALL)
     private List<Function> functions;

    @Override
    public String getRoleCode() {
        return roleCode;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List getUserRoles() {
        return userRoles;
    }

    @Override
    public List getFunctions() {
        return functions;
    }


    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setFunctions(List<Function> functions) {
        this.functions = functions;
    }

    public void setUserRoles(List<UserRole> userRoles) {
        this.userRoles = userRoles;
    }
}
