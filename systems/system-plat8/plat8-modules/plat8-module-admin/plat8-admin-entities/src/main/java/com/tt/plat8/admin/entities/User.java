package com.tt.plat8.admin.entities;



import com.tt.plat.core.impl.PersistentEntity;
import com.tt.plat8.admin.api.IUser;

import javax.persistence.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-17
 * Time: 下午4:27
 * 用户实体
 */
@Entity
@Table(name = "P_USER")
public class User extends PersistentEntity implements IUser {

    private String username; //用户账号

    private String name; //用户名称

    private String email;//用户邮箱

    private String password; //用户密码



    @OneToMany(fetch= FetchType.LAZY,cascade = CascadeType.ALL, mappedBy="user")
    private List<UserRole> userRoles; //用户角色

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getEmail() {
        return email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public List getUserRoles() {
        return userRoles;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setUserRoles(List<UserRole> userRoles) {
        this.userRoles = userRoles;
    }


}
