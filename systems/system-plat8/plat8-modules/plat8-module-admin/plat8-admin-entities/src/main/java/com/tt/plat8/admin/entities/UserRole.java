package com.tt.plat8.admin.entities;




import com.tt.plat.core.impl.PersistentEntity;
import com.tt.plat8.admin.api.IRoleCode;
import com.tt.plat8.admin.api.IUser;
import com.tt.plat8.admin.api.IUserRole;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-18
 * Time: 下午12:55
 * 用户角色实体
 */
@Entity
@Table(name = "P_USER_ROLE")
public class UserRole extends PersistentEntity implements IUserRole {

    @ManyToOne
    private User user;


    @ManyToOne
    private RoleCode roleCode;



    @Override
    public IUser getUser() {
        return user;
    }

    @Override
    public IRoleCode getRoleCode() {
        return roleCode;
    }



    public void setUser(IUser user) {
        this.user = (User) user;
    }

    public void setRoleCode(IRoleCode roleCode) {
        this.roleCode = (RoleCode) roleCode;
    }



}
