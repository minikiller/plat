package com.tt.plat8.admin.biz;



import com.tt.plat.core.impl.GenericOpenJpaDao;
import com.tt.plat8.admin.api.IUser;
import com.tt.plat8.admin.api.IUserLoginDao;
import com.tt.plat8.admin.api.IUserRole;
import com.tt.plat8.admin.entities.User;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-18
 * Time: 下午3:58
 * To change this template use File | Settings | File Templates.
 */
public class UserLoginDaoImpl extends GenericOpenJpaDao<IUser, Long> implements IUserLoginDao {

    public void setEntityManager(EntityManager em) {
        super.setEntityManager(em);
        super.setPersistentClass(User.class);

    }


    @Override
    public IUser getUser(String username) {
        System.out.println("----------"+this.entityManager);
        IUser user = this.findUnique("select u from User u where u.username=?1", username);
        if(user==null){
            return null;
        }
        List<IUserRole> userRoles = user.getUserRoles();

        if (userRoles != null) {
            for (IUserRole userRole : userRoles) {
                    userRole.getRoleCode().getFunctions();
            }
        }

        return user;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
