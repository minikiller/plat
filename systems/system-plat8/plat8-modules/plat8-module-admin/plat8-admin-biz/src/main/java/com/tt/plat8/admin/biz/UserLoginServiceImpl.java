package com.tt.plat8.admin.biz;



import com.tt.plat.core.web.api.modules.ISystemModule;
import com.tt.plat.core.web.imp.manager.SystemModuleManager;
import com.tt.plat8.admin.api.IUser;
import com.tt.plat8.admin.api.IUserLoginDao;
import com.tt.plat8.admin.api.IUserLoginService;
import com.tt.plat8.admin.security.UserSession;


import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-17
 * Time: 下午5:10
 * To change this template use File | Settings | File Templates.
 */
public class UserLoginServiceImpl implements IUserLoginService {


    private IUserLoginDao userLoginDao;

    public IUserLoginDao getUserLoginDao() {
        return userLoginDao;
    }

    public void setUserLoginDao(IUserLoginDao userLoginDao) {
        this.userLoginDao = userLoginDao;
    }

    public UserLoginServiceImpl() {

    }

    @Override
    public int login(String username, String password) {

       //
        IUser user  =  userLoginDao.getUser(username);
        if (user == null) {

            return USERNAME_NOT_EXIST;
        }

        if (encrypt(password).equals(user.getPassword())) {
            UserSession userSession = (UserSession) UserSession.get();
            userSession.setObj(user);
            return SUCCESS;
        } else {
            return PASSWORD_ERROR;
        }

    }

    @Override
    public ISystemModule getSelectedSystemModule() {
       List<ISystemModule> systemModuleList = SystemModuleManager.getInstall().getSystemModuleList(SystemModuleManager.PLAT8_SYSTEM_MODULE);
       if(systemModuleList!=null&&systemModuleList.size()>0){

           return systemModuleList.get(0);
       }
        return null;
    }

    private String encrypt(String text) {

        return text;
    }
}
