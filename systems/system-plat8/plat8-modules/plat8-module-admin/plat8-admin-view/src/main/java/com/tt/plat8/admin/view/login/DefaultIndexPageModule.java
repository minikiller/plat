package com.tt.plat8.admin.view.login;


import com.tt.plat.core.web.api.modules.IndexPageModule;
import org.apache.wicket.Page;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-17
 * Time: 下午5:58
 * To change this template use File | Settings | File Templates.
 */
public class DefaultIndexPageModule implements IndexPageModule {

    private Class<Page> page;
    @Override
    public Class<? extends Page> getPageClass() {
        return page;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setPage(Class<Page> page) {
        this.page = page;
    }

    @Override
    public String getName() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getIcon() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getTag() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }


}
