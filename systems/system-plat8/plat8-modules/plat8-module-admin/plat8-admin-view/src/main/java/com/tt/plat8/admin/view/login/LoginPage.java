package com.tt.plat8.admin.view.login;

import com.tt.plat.core.web.api.modules.IPageModule;

import com.tt.plat.core.web.imp.MessageFeebackPanel;
import com.tt.plat.core.web.imp.Plat;
import com.tt.plat8.admin.api.IUserLoginService;
import org.apache.aries.blueprint.annotation.Reference;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.util.io.IClusterable;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-17
 * Time: 下午2:35
 * To change this template use File | Settings | File Templates.
 */
public class LoginPage extends WebPage {

     private Form<Bean> loginForm;

    private Bean bean = new Bean();

    @Named
    @Inject
    @Reference(id="homeProvider",serviceInterface = IPageModule.class,filter = "(tag=home)")
    private IPageModule pageModule;

    @Named
    @Inject
    @Reference(id="userLoginService",serviceInterface = IUserLoginService.class)
    private IUserLoginService userLoginService;

    public LoginPage(){

        loginForm = new Form<Bean>("loginForm",new CompoundPropertyModel<Bean>(bean));

        final MessageFeebackPanel feedback = new MessageFeebackPanel(
                "errors");
        feedback.setOutputMarkupId(true);

        loginForm.add(feedback);



        //用户名
        TextField<String> usernameFd = new TextField<String>("username",new PropertyModel<String>(loginForm.getModel(),"username"));
        usernameFd.setRequired(true);

        PasswordTextField password = new PasswordTextField("password",
                new PropertyModel<String>(loginForm.getModel(), "password"));
        password.setRequired(true);

        loginForm.add(usernameFd);
        loginForm.add(password);


        Button loginButton = new Button("loginButton"){
            @Override
            public void onSubmit() {
//                PageModule homePageProvider=null;
//                try {
//                    homePageProvider  = JNDIHelper.getJNDIServiceForName(PageModule.class.getName()+"/(tag=home)");
//                } catch (IOException e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//                }

               int iresultCode = userLoginService.login(bean.getUsername(),bean.getPassword());
                if(iresultCode==IUserLoginService.SUCCESS){
                     Plat.getInstall().setCurrSystem(userLoginService.getSelectedSystemModule());
                    setResponsePage(pageModule.getPageClass());
                } else if(iresultCode==IUserLoginService.USERNAME_NOT_EXIST){
                    ( (MessageFeebackPanel)loginForm.get("errors") ).error("用户不存在！");
                } else if(iresultCode==IUserLoginService.PASSWORD_ERROR){
                    ( (MessageFeebackPanel)loginForm.get("errors") ).error("密码错误！");
                }else {
                    ( (MessageFeebackPanel)loginForm.get("errors") ).error("未知错误！");
                }

            }
        };

        loginForm.add(loginButton);

        add(loginForm);
    }

}

class Bean implements IClusterable {
    private String username;

    private String password;

    String getUsername() {
        return username;
    }

    void setUsername(String username) {
        this.username = username;
    }

    String getPassword() {
        return password;
    }

    void setPassword(String password) {
        this.password = password;
    }
}
