package com.tt.plat8.admin.api;


import com.tt.plat.core.api.persistence.IPersistentEntity;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-20
 * Time: 下午2:12
 * 功能接口
 */
public interface IFunction extends IPersistentEntity {

    /**
     * 功能名称
     * @return
     */
    public String getName();

    /**
     * 功能图标
     * @return
     */
    public String getIcon();

    /**
     * 功能类名（包括包名）
     * @return
     */
    public String getClassName();

    /**
     * 功能描叙
     * @return
     */
    public String getDescript();


}
