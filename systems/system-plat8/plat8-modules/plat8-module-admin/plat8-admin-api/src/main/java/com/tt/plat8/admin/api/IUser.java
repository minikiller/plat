package com.tt.plat8.admin.api;


import com.tt.plat.core.api.persistence.IPersistentEntity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-17
 * Time: 下午4:11
 * To change this template use File | Settings | File Templates.
 */
public interface IUser extends IPersistentEntity {

    String getName();

    String getEmail();

    String getUsername();

    String getPassword();

    public List<IUserRole> getUserRoles();

}
