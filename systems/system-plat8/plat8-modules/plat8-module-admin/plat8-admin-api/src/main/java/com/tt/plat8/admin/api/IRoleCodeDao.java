package com.tt.plat8.admin.api;



import com.tt.plat.core.api.persistence.IGenericDao;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-20
 * Time: 下午4:12
 * 角色代码DAO
 */
public interface IRoleCodeDao extends IGenericDao<IRoleCode,Long> {

    public List<IRoleCode> findAllRoleCode();
}
