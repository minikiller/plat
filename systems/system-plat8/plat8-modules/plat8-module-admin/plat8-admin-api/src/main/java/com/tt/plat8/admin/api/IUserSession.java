package com.tt.plat8.admin.api;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-19
 * Time: 上午11:24
 * To change this template use File | Settings | File Templates.
 */
public interface IUserSession {

    /**
     * 获取用户信息
     * @return
     */
    public IUser getUser();

    /**
     * 设置用户信息
     * @param user
     */
    public void setUser(IUser user);


    /**
     * 是否有给定的角色
     * @param role
     * @return
     */
    public boolean hasRole(String role);

}
