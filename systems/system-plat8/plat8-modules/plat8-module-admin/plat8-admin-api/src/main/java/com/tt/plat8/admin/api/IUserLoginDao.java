package com.tt.plat8.admin.api;


import com.tt.plat.core.api.persistence.IGenericDao;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-18
 * Time: 下午3:58
 * To change this template use File | Settings | File Templates.
 */
public interface IUserLoginDao extends IGenericDao<IUser,Long> {


    public IUser getUser(String username);
}
