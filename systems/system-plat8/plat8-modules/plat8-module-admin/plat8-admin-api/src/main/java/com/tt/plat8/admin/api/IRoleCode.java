package com.tt.plat8.admin.api;



import com.tt.plat.core.api.persistence.IPersistentEntity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-19
 * Time: 上午11:39
 * To change this template use File | Settings | File Templates.
 */
public interface IRoleCode extends IPersistentEntity {


    /**
     * 角色代码
     * @return
     */
    public String getRoleCode();

    /**
     * 角色代码描述
     * @return
     */
    public String getDescription();

    /**
     * 获取用户角色
     * @return
     */
    public List<IUserRole> getUserRoles();


    /**
     * 获取功能集合
     * @return
     */
    public List<IFunction> getFunctions();
}
