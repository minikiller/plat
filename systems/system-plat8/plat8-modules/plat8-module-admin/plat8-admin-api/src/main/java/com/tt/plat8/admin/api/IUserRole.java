package com.tt.plat8.admin.api;


import com.tt.plat.core.api.persistence.IPersistentEntity;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-19
 * Time: 上午11:38
 * To change this template use File | Settings | File Templates.
 */
public interface IUserRole extends IPersistentEntity {

    /**
     * 获取用户信息
     * @return
     */
    public IUser getUser();

    /**
     * 获取角色代码
     * @return
     */
    public IRoleCode getRoleCode();


}
