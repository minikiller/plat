package com.tt.plat8.admin.api;


import com.tt.plat.core.web.api.modules.ISystemModule;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-17
 * Time: 下午5:07
 * 用户登录服务
 */
public interface IUserLoginService {

    /**
     * 用户名不存在
     */
    public final static int USERNAME_NOT_EXIST=-1;

    /**
     * 密码错误
     */
    public final static int PASSWORD_ERROR=-2;

    /**
     * 位置错误
     */
    public final static int UNKOWN_ERROR=-99;

    /**
     * 成功
     */
    public final static int SUCCESS=1;



    /**
     * 用户登录
     * @param username 用户名
     * @param password  用户密码
     * @return
     */
    public int login(String username, String password);


    /**
     * 获取被选择的系统模块
     * @return
     */
    public ISystemModule getSelectedSystemModule();
}
