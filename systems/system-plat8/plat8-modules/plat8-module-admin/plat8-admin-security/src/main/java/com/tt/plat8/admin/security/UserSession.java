package com.tt.plat8.admin.security;

import com.tt.plat.core.web.api.modules.IPlatSession;

import com.tt.plat8.admin.api.IUser;
import com.tt.plat8.admin.api.IUserRole;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.request.Request;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-19
 * Time: 上午11:30
 * 用户会话实体
 */
public class UserSession extends WebSession implements IPlatSession {


    private IUser user;

    private Roles roles;

    public UserSession(Request request) {
        super(request);

    }

    @Override
    public Object getObj() {

        return this.user;
    }

    @Override
    public void setObj(Object user) {
        this.user=(IUser)user;
        this.roles = new Roles();
        fillRoles(this.user);
    }

    private void fillRoles(IUser user){
         List<IUserRole> userRoles = user.getUserRoles();
        if(userRoles!=null){
              for(IUserRole userRole : userRoles){
                    roles.add(userRole.getRoleCode().getRoleCode());
              }
        }
    }

    public boolean hasAnyRole(Roles roles)
    {
        return this.roles.hasAnyRole(roles);
    }


    public boolean hasRole(String role)
    {
        return roles.hasRole(role);
    }
}
