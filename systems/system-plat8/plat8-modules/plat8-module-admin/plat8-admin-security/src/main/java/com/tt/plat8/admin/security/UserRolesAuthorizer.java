package com.tt.plat8.admin.security;

import org.apache.wicket.Session;
import org.apache.wicket.authroles.authorization.strategies.role.IRoleCheckingStrategy;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-19
 * Time: 下午1:35
 * To change this template use File | Settings | File Templates.
 */
public class UserRolesAuthorizer implements IRoleCheckingStrategy {
    @Override
    public boolean hasAnyRole(Roles strings) {
        UserSession userSession = (UserSession) Session.get();

        return userSession.hasAnyRole(strings);
    }
}
