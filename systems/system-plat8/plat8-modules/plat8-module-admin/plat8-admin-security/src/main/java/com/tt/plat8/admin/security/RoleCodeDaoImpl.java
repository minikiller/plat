package com.tt.plat8.admin.security;



import com.tt.plat.core.impl.GenericOpenJpaDao;
import com.tt.plat8.admin.api.IRoleCode;
import com.tt.plat8.admin.api.IRoleCodeDao;
import com.tt.plat8.admin.entities.RoleCode;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-2-20
 * Time: 下午4:17
 * To change this template use File | Settings | File Templates.
 */
public class RoleCodeDaoImpl extends GenericOpenJpaDao<IRoleCode,Long> implements IRoleCodeDao {

    public void setEntityManager(EntityManager em) {
        super.setEntityManager(em);
        super.setPersistentClass(RoleCode.class);

    }
    @Override
    public List<IRoleCode> findAllRoleCode() {
        List<IRoleCode> roleCodeList = this.getAll();
        if(roleCodeList!=null){
              for(IRoleCode roleCode : roleCodeList){
                  roleCode.getFunctions();
              }
        }
        return this.getAll();
    }
}
