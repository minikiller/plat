package com.tt.plat8.base;


import com.tt.plat.core.web.api.modules.IPlatSystemModule;

/**
 * Created with IntelliJ IDEA.
 * User: tangtao
 * Date: 14-3-7
 * Time: 下午12:38
 * To change this template use File | Settings | File Templates.
 */
public class SystemModule implements IPlatSystemModule {
    @Override
    public String[] getTargetTag() {
        return new String[]{"8plat"};
    }

    @Override
    public String getNo() {
        return "123456";
    }

    @Override
    public String getName() {
        return "8plat系统";
    }

    @Override
    public String getIcon() {
        return null;
    }

    @Override
    public String getTag() {
        return "system.8plat";
    }


}
